import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Factors {
    public static List<Integer> factor(long number){
        HashSet<Integer> factorsSorted = new HashSet<>();
        List<Integer> factors = new ArrayList<>();
        long n = number;
        for (int i = 2; i <= n; i++) {
            while (n % i == 0) {
                factorsSorted.add(i);
                n /= i;
            }
        }
        factors.addAll(factorsSorted);

        return factors;
    }
}
