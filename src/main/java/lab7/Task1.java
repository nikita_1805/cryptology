package lab7;

import java.awt.*;
import java.math.BigInteger;

public class Task1 {
    public static void main(String[] args) {
        int a = -1;
        BigInteger p = new BigInteger("751");
        Operations op = new Operations(p,a);
        Point P = new Point(67, 667);
        Point Q = new Point(53, 474);
        Point R = new Point(105, 382);
        Point PP = op.doublePoint(P);
        System.out.println(PP);
        Point QQ = op.doublePoint(Q);
        Point tripleQ = op.addPoint(QQ, Q);
        System.out.println(tripleQ);
        Point PplusQ = op.addPoint(PP, tripleQ);
        Point res = op.subtractPoint(PplusQ, R);
        System.out.println(res);
    }
}


