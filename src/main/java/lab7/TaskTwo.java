package lab7;

import java.awt.*;
import java.math.BigInteger;

public class TaskTwo {
    public static void main(String[] args) {
        int a = -1;
        BigInteger p = new BigInteger("751");
        Operations op = new Operations(p, a);

        Point P = new Point(44, 385);
        Point P64 = P, P32 = P, P16 = P;
        for (int i = 0; i < 6; i++) {
            if (i < 4) P16 = op.doublePoint(P16);
            if (i < 5) P32 = op.doublePoint(P32);
            P64 = op.doublePoint(P64);
        }
        Point P1P2 = op.addPoint(P64, P32);
        Point P2P3 = op.addPoint(P1P2, P16);
        Point result = op.addPoint(P2P3, P);

        System.out.println(result);
    }
}
