package lab7;

import java.awt.*;
import java.math.BigInteger;
import java.util.ArrayList;

public class TaskFour {
    public static void main(String[] args) {
        int a = -1;
        BigInteger p = new BigInteger("751");
        Operations op = new Operations(p,a);
        Point Q = new Point();
        Point P = new Point(3,5);
        Point P4 = P, P2 = P, P8 = P, P16 = P, P32 = P;
        Point [] M = new Point[8];
        Point [] R = {new Point(295,219),new Point(585,540),new Point(573,583),new Point(694,581),
        new Point(585,540),new Point(36,664), new Point(646,706), new Point(701,570)};
        Point [] kP = {new Point(188,93),new Point(179,275),new Point(440,539), new Point(16,416),
        new Point(179,275), new Point(286,136), new Point(618,206), new Point(377,456)};
        Point akP [] = new Point[8];

        for (int i = 0, kPLength = kP.length; i < kPLength; i++) {
            Point point = kP[i];

            for (int j = 0; j < 1; j++){
                    P2 = op.doublePoint(point);

                    P4 = op.doublePoint(P2);

                    P8 = op.doublePoint(P4);
                    P16 = op.doublePoint(P8);

                    P32 = op.doublePoint(P16);
            }
            Q = op.addPoint(P2,P4);
            Q = op.addPoint(Q,P16);
            Q = op.addPoint(Q,P32);
            akP[i] = Q;
            System.out.println(akP[i]);
        }
        System.out.println("\n");
        for (int i = 0; i < R.length; i++) {
            M[i] = op.subtractPoint(R[i],akP[i]);
            System.out.println(M[i]);
        }
    }
}
