package lab7;

import java.awt.*;
import java.math.BigInteger;

public class Operations {
    private  BigInteger p;
    private  int a;

    public Operations(BigInteger p, int a) {
        this.p = p;
        this.a = a;
    }

    public Point doublePoint(Point point){
        Long chisl = Long.valueOf(3*point.x*point.x + a);
        Long znam = Long.valueOf(2*point.y);
        BigInteger znamen = new BigInteger(znam.toString()).modInverse(p);
        BigInteger lambda = new BigInteger(chisl.toString()).multiply(znamen).mod(p);

        int lambd = lambda.intValue();
        Long x = Long.valueOf((lambd*lambd - 2*point.x));
        Long y = Long.valueOf(lambd*(point.x - x) - point.y);
        BigInteger res = new BigInteger(x.toString()).mod(p);
        int xx = res.intValue();
        res = new BigInteger(y.toString()).mod(p);
        int yy = res.intValue();
        return new Point(xx,yy);
    }
    public Point addPoint(Point P, Point Q){
        BigInteger znamen;
        Long chisl = Long.valueOf(Q.y - P.y);
        Long znam = Long.valueOf(Q.x - P.x);
        if (znam == 0) znamen = new BigInteger(znam.toString());
        else znamen = new BigInteger(znam.toString()).modInverse(p);
        BigInteger lambda = new BigInteger(chisl.toString()).multiply(znamen).mod(p);
        int lambd = lambda.intValue();
        Long x = Long.valueOf(lambd*lambd - P.x - Q.x);
        Long y = lambd*(P.x - x) - P.y;
        BigInteger res = new BigInteger(x.toString()).mod(p);
        int xRes = res.intValue();
        res = new BigInteger(y.toString()).mod(p);
        int yRes = res.intValue();

        return new Point(xRes,yRes);
    }
    public Point subtractPoint(Point P, Point Q){
        Long qY = Long.valueOf(-Q.y);
       // BigInteger minY = new BigInteger(qY.toString()).modInverse(p);
       // int miny = minY.intValue();
        Point minQ = new Point(Q.x, Math.toIntExact(qY));
        Long chisl = Long.valueOf(minQ.y - P.y);
        Long znam = Long.valueOf(minQ.x - P.x);
        BigInteger znamen = new BigInteger(znam.toString()).modInverse(p);
        BigInteger lambda = new BigInteger(chisl.toString()).multiply(znamen).mod(p);
        int lambd = lambda.intValue();
        Long x = Long.valueOf(lambd*lambd - P.x - minQ.x);
        Long y = lambd*(P.x - x) - P.y;
        BigInteger res = new BigInteger(x.toString()).mod(p);
        int xRes = res.intValue();
        res = new BigInteger(y.toString()).mod(p);
        int yRes = res.intValue();

        return new Point(xRes,yRes);
    }
}
