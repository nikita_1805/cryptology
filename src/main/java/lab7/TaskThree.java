package lab7;

import java.awt.*;
import java.math.BigInteger;
import java.util.ArrayList;

public class TaskThree {
    public static void main(String[] args) {
        int a = -1;
        BigInteger p = new BigInteger("751");
        Operations op = new Operations(p,a);
        Point R;
        Point Q = new Point(618,206);
        Point P = new Point(0,1);
        Point [] M = {new Point(238,175),new Point(247,485),
                new Point(251,245), new Point(247,266), new Point(243,87), new Point(228,271)};
        Point P4 = P, P2 = P, P8 = P, P16 = P;
        Point Q4 = Q, Q2 = Q, Q8 = Q, Q16 = Q;
        for (int i = 0; i < 2; i++){
            P4 = op.doublePoint(P4);
            Q4 = op.doublePoint(Q4);
        }
        P4 = op.addPoint(P4, P);
        Q4 = op.addPoint(Q4,Q);
        R = op.addPoint(M[0],Q4);

        System.out.println(P4 + " " + R);

        P4 = P; Q4 = Q;

        for (int i = 0; i < 4; i++){
            if (i < 1) {
                P2 = op.doublePoint(P2);
                Q2 = op.doublePoint(Q2);
            }
            Q16 = op.doublePoint(Q16);
            P16 = op.doublePoint(P16);
        }
        Point pRes = op.addPoint(P2,P16);
        pRes = op.addPoint(pRes,P);
        Point qRes = op.addPoint(Q2,Q16);
        qRes = op.addPoint(qRes,Q);
        R = op.addPoint(qRes,M[1]);

        System.out.println(pRes + " " + R);

        P4 = P; P2 = P; P8 = P; P16 = P;
        Q4 = Q; Q2 = Q; Q8 = Q; Q16 = Q;

        for (int i = 0; i < 3; i++){
            P8 = op.doublePoint(P8);
            Q8 = op.doublePoint(Q8);
        }
//        P8 = op.addPoint(P8, P);
//        Q8 = op.addPoint(Q8,Q);
        R = op.addPoint(M[2],Q8);

        System.out.println(P8 + " " + R);

        P4 = P; P2 = P; P8 = P; P16 = P;
        Q4 = Q; Q2 = Q; Q8 = Q; Q16 = Q;

        P2 = op.doublePoint(P2);
        Q2 = op.doublePoint(Q2);
//        P2 = op.addPoint(P2, P);
//        Q2 = op.addPoint(Q2,Q);
        R = op.addPoint(M[3],Q2);

        System.out.println(P2 + " " + R);


        P4 = P; P2 = P; P8 = P; P16 = P;
        Q4 = Q; Q2 = Q; Q8 = Q; Q16 = Q;

        for (int i = 0; i < 2; i++){
            P4 = op.doublePoint(P4);
            Q4 = op.doublePoint(Q4);
        }
        P4 = op.addPoint(P4, P);
        Q4 = op.addPoint(Q4,Q);
        R = op.addPoint(M[4],Q4);

        System.out.println(P4 + " " + R);

        P4 = P; P2 = P; P8 = P; P16 = P;
        Q4 = Q; Q2 = Q; Q8 = Q; Q16 = Q;

        for (int i = 0; i < 2; i++){
            if (i < 1){
            P2 = op.doublePoint(P2);
            Q2 = op.doublePoint(Q2);
            }
            Q4 = op.doublePoint(Q4);
            P4 = op.doublePoint(P4);
        }

        pRes = op.addPoint(P2,P4);
        qRes = op.addPoint(Q2,Q4);
        R = op.addPoint(qRes,M[5]);

        System.out.println(pRes + " " + R);
    }
}
