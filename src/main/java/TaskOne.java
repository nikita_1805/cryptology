import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TaskOne {
    public static Random generator = new Random();
    public static void main(String[] args) {
        BigInteger p,g,a,Yp,h,r;
        List<Integer> factors = new ArrayList<>();
        List<BigInteger> text = new ArrayList<>();
        List<BigInteger> C1 = new ArrayList<>();
        List<BigInteger> C2 = new ArrayList<>();
        List<BigInteger> M = new ArrayList<>();
        text.add(new BigInteger("3018")); text.add(new BigInteger("7014"));
        text.add(new BigInteger("9129")); text.add(new BigInteger("2001"));
        text.add(new BigInteger("951"));text.add(new BigInteger("8310"));
        text.add(new BigInteger("1112"));text.add(new BigInteger("925"));
        p = new BigInteger("9421");
        r = new BigInteger("0");
        Yp = p.subtract(new BigInteger("1"));
        factors = Factors.factor(Yp.longValue());
        g = new BigInteger("229");
        System.out.println("Факторизованная функция Эйлера от модуля: " + factors);
        System.out.println("Factorized p:" + factors);
        System.out.println("A power of (p-1)/factorized p:");
        System.out.println(g.modPow(new BigInteger("4710"),p));
        System.out.println(g.modPow(new BigInteger("3140"),p));
        System.out.println(g.modPow(new BigInteger("1884"),p));
        System.out.println(g.modPow(new BigInteger("60"),p));

        a = BigInteger.valueOf((long)generator.nextInt(p.intValue()-2));
        System.out.println("а = " + a);
        h  = g.modPow(a,p);
        System.out.println("h = " + h);
        for (int i =0; i < text.size(); i++){
            r = BigInteger.valueOf((long)generator.nextInt(p.intValue()-1));
            C1.add(g.modPow(r,p));
            C2.add(text.get(i).multiply(h.modPow(r,p)).mod(p));
        }

        System.out.println("C1 = " + C1);
        System.out.println("C2 = " + C2);

        for (int i = 0; i < text.size(); i++){
            M.add(C2.get(i).multiply(C1.get(i).modPow(a,p).modInverse(p)).mod(p));
        }
        System.out.println("M = " + M);

    }
}
