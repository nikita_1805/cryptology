import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class TaskTwo {
    public static void main(String[] args) {
        BigInteger p,g,a,C1,h,r;
        g = new BigInteger("6782");
        p = new BigInteger("7162619");
        C1 = new BigInteger("6960429");
        a = new BigInteger("514");
        List<BigInteger> M = new ArrayList<>();
        List<BigInteger> C2 = new ArrayList<>();
        C2.add(new BigInteger("2962666"));C2.add(new BigInteger("6436388"));
        C2.add(new BigInteger("6374477"));C2.add(new BigInteger("5150334"));
        C2.add(new BigInteger("5735554"));C2.add(new BigInteger("2227198"));
        h = g.modPow(a,p);
        System.out.println("h = " + h);
        for (int i = 0; i < C2.size(); i++){
            M.add(C2.get(i).multiply(C1.modPow(a,p).modInverse(p)).mod(p));
        }
        System.out.println("M = " + M);
    }
}
