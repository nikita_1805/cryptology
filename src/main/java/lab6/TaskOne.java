package lab6;

import java.math.BigInteger;

public class TaskOne {
    public static void main(String[] args) {
        BigInteger p,g,y,M,a,b,result1,result2;
        p = new BigInteger("9993209");
        g = new BigInteger("3137");
        M = new BigInteger("15569");
        a = new BigInteger("6351010");
        b = new BigInteger("7261212");
        y = new BigInteger("7067285");
        result1 = y.modPow(a,p).multiply(a.modPow(b,p)).mod(p);
        result2 = g.modPow(M,p);
        System.out.println("Проверка подписи для первого сообщения:");
        System.out.println("P1 = " + result1 + "\t" + "P2 = " + result2);
        System.out.println("Р1 = Р2: " + result1.equals(result2));
        M = new BigInteger("15803");
        a = new BigInteger("3772984");
        b = new BigInteger("4552517");
        result1 = y.modPow(a,p).multiply(a.modPow(b,p)).mod(p);
        result2 = g.modPow(M,p);
        System.out.println("Проверка подписи для второго сообщения:");
        System.out.println("P1 = " + result1 + "\t" + "P2 = " + result2);
        System.out.println("Р1 = Р2: " + result1.equals(result2));
    }
}
