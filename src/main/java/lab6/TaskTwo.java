package lab6;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class TaskTwo {
    public static void main(String[] args) {
        BigInteger p,g,x,r,H0,y,a,b;
        List<BigInteger> message = new ArrayList<>();
        p = new BigInteger("97685839");
        g = new BigInteger("1103");
        x = new BigInteger("2767");
        r = new BigInteger("439");
        message.add(new BigInteger("0015"));
        message.add(new BigInteger("3024"));
        message.add(new BigInteger("0033"));
        message.add(new BigInteger("1133"));
        message.add(new BigInteger("1816"));
        message.add(new BigInteger("0603"));
        message.add(new BigInteger("0033"));
        System.out.println("Подписываемое сообщение: " + message);
        H0 = new BigInteger("1234");
        y = g.modPow(x,p);
        for (int i = 0; i < message.size(); i++) {
            H0 = H0.add(message.get(i)).modPow(new BigInteger("2"), p);
        }
        a = g.modPow(r,p);
        b = H0.subtract(a.multiply(x)).multiply(r.modInverse(p)).mod(p.subtract(new BigInteger("1")));
        System.out.println("Подпись сообщения М:");
        System.out.println("H(M) = " + H0);
        System.out.println("a = " + a);
        System.out.println("b = " + b);

    }
}
