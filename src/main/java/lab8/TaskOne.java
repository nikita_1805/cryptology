package lab8;

import lab7.Operations;

import java.awt.*;
import java.math.BigInteger;

public class TaskOne {
    public static void main(String[] args) {
        int a = -1;
        BigInteger p = new BigInteger("751");
        Operations op = new Operations(p,a);
        BigInteger n = new BigInteger("13");
        BigInteger H = new BigInteger("7");
        BigInteger d = new BigInteger("3");
        BigInteger k = new BigInteger("7");

        Point G = new Point(416,55);
        Point G2 = G, G4 = G, kG;

        G2 = op.doublePoint(G);
        G4 = op.doublePoint(G2);
        kG = op.addPoint(G,G2);
        kG = op.addPoint(kG,G4);

        System.out.println(kG);

        Long xkG  = Long.valueOf(kG.x);
        BigInteger R = new BigInteger(xkG.toString()).mod(n);
        System.out.println(R);

        BigInteger S = k.modInverse(n).multiply(H.add(d.multiply(R))).mod(n);
        System.out.println(S);


    }
}
