package lab8;

import lab7.Operations;

import java.awt.*;
import java.math.BigInteger;

public class TaskTwo {
    public static void main(String[] args) {
        int a = -1;
        BigInteger p = new BigInteger("751");
        Operations op = new Operations(p,a);
        BigInteger n = new BigInteger("13");
        BigInteger H = new BigInteger("8");
        BigInteger R = new BigInteger("11");
        BigInteger S = new BigInteger("6");
        Point G = new Point(562,89);
        Point Q = new Point(596,318);

        Point G2 = G, G4 = G, G8 = G, U1G = G;
        Point Q2 = Q,  U2Q = Q;

        Point result;

        BigInteger U1 = H.multiply(S.modInverse(n)).mod(n);
        BigInteger U2 = R.multiply(S.modInverse(n)).mod(n);

        System.out.println( "U1 = " + U1);
        System.out.println("U2 = " + U2);

       G2 = op.doublePoint(G);
       G4 = op.doublePoint(G2);
       G8 = op.doublePoint(G4);
       U1G = op.addPoint(G8,G2);

        System.out.println("U1G = " + U1G);

       Q2 = op.doublePoint(Q);
       U2Q = op.doublePoint(Q2);

        System.out.println("U2Q = " + U2Q);

        result = op.addPoint(U1G,U2Q);

        BigInteger res = new BigInteger("734").mod(n);

        System.out.println("Result = " + result);

        System.out.println("x = " + res + "\t" + "R = " + R);

    }
}
