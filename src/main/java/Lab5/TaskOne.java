package Lab5;

import java.math.BigInteger;
import java.util.ArrayList;

public class TaskOne {
    public static void main(String[] args) {
        BigInteger IV, n, e, x, x2, IV2;

        ArrayList<BigInteger> Hi = new ArrayList<>();
        ArrayList<BigInteger> Mi = new ArrayList<>();

        Mi.add(new BigInteger("2119"));
        Mi.add(new BigInteger("1115"));
        Mi.add(new BigInteger("3017"));
        Mi.add(new BigInteger("0033"));
        Mi.add(new BigInteger("1618"));
        Mi.add(new BigInteger("0200"));
        System.out.println("First message: " + Mi);
        Hi.add(new BigInteger("2123"));
        Hi.add(new BigInteger("2230"));
        Hi.add(new BigInteger("3321"));
        Hi.add(new BigInteger("1920"));
        Hi.add(new BigInteger("0002"));
        Hi.add(new BigInteger("1033"));
        System.out.println("Second message: " + Hi);

        x = new BigInteger("1038901");
        x2 = new BigInteger("35972389");

        IV = new BigInteger("1234");
        IV2 = new BigInteger("1234");
        n = new BigInteger("68529469");
        e = new BigInteger("3617");

        for (int i = 0; i < Mi.size(); i++) {
            IV = IV.add(Mi.get(i)).modPow(new BigInteger("2"), n);
            IV2 = IV2.add(Hi.get(i)).modPow(new BigInteger("2"), n);
        }
        System.out.println("Hash-function: " + IV + "\t" + IV2);
        x = x.modPow(e, n);
        x2 = x2.modPow(e, n);
        System.out.println("S^e(mod n): " + x + "\t" + x2);
        System.out.println(x.equals(IV));
        System.out.println(x2.equals(IV2));
    }
}
