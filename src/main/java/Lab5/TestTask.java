package Lab5;

import java.math.BigInteger;
import java.util.ArrayList;

public class TestTask {
    public static void main(String[] args) {
        BigInteger IV, n, e, x, x2, IV2;

        ArrayList<BigInteger> Hi = new ArrayList<>();
        ArrayList<BigInteger> Mi = new ArrayList<>();

        Mi.add(new BigInteger("2910"));
        Mi.add(new BigInteger("2122"));
        Mi.add(new BigInteger("0033"));
        Mi.add(new BigInteger("1920"));
        Mi.add(new BigInteger("0002"));
        Mi.add(new BigInteger("500"));
        System.out.println("First message: " + Mi);
        Hi.add(new BigInteger("2110"));
        Hi.add(new BigInteger("1602"));
        Hi.add(new BigInteger("1815"));
        Hi.add(new BigInteger("3303"));
        Hi.add(new BigInteger("1120"));
        Hi.add(new BigInteger("1033"));
        System.out.println("Second message: " + Hi);

        x = new BigInteger("1656295");
        x2 = new BigInteger("14780078");

        IV = new BigInteger("1234");
        IV2 = new BigInteger("1234");
        n = new BigInteger("28537837");
        e = new BigInteger("547");

        for (int i = 0; i < Mi.size(); i++) {
            IV = IV.add(Mi.get(i)).modPow(new BigInteger("2"), n);
            IV2 = IV2.add(Hi.get(i)).modPow(new BigInteger("2"), n);
        }
        System.out.println("Hash-function: " + IV + "\t" + IV2);
        x = x.modPow(e, n);
        x2 = x2.modPow(e, n);
        System.out.println("S^e(mod n): " + x + "\t" + x2);
        System.out.println(x.equals(IV));
        System.out.println(x2.equals(IV2));
    }
}
