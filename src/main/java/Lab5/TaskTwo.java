package Lab5;

import java.math.BigInteger;
import java.util.ArrayList;

public class TaskTwo {
    public static void main(String[] args) {
        BigInteger p, q, IV, e, n, d, s, Yn;
        ArrayList<BigInteger> H = new ArrayList<>();
        ArrayList<BigInteger> message = new ArrayList<>();
        message.add(new BigInteger("0015"));
        message.add(new BigInteger("3024"));
        message.add(new BigInteger("0033"));
        message.add(new BigInteger("1133"));
        message.add(new BigInteger("1816"));
        message.add(new BigInteger("0603"));
        message.add(new BigInteger("0033"));
        BigInteger H0 = new BigInteger("1234");
        System.out.println("Message: " + message);
        p = new BigInteger("6287");
        q = new BigInteger("9277");
        n = p.multiply(q);
        System.out.println("Module: " + n);
        e = new BigInteger("379");
        Yn = p.subtract(new BigInteger("1")).multiply(q.subtract(new BigInteger("1")));
        d = e.modInverse(Yn);
        System.out.println("Secret key: " + d);
        for (int i = 0; i < message.size(); i++) {
            H0 = H0.add(message.get(i)).modPow(new BigInteger("2"), n);
            H.add(H0);
        }
        System.out.println("Hash=function: " + H0);
        s = H0.modPow(d, n);
        System.out.println("Sign: " + s);
    }
}
