package lab9;

import java.math.BigInteger;

public class TaskThree {
    public static void main(String[] args) {
        BigInteger ex = new BigInteger("130");
        System.out.println(ex.modInverse(new BigInteger("131")));
    }
}
