package lab9;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class TaskOne {
    public static void main(String[] args) {
        BigInteger p = new BigInteger("48544121");
        List<BigInteger> r = new ArrayList<>();
        BigInteger S, inverse;
        r.add(new BigInteger("202"));
        r.add(new BigInteger("2023"));
        r.add(new BigInteger("22333"));
        r.add(new BigInteger("33222"));
        r.add(new BigInteger("332333"));
        r.add(new BigInteger("5555555"));
        r.add(new BigInteger("322211"));
        List<BigInteger> c = new ArrayList<>();
        c.add(new BigInteger("12792550"));
        c.add(new BigInteger("21995542"));
        c.add(new BigInteger("23778838"));
        c.add(new BigInteger("6097773"));
        c.add(new BigInteger("36731762"));
        c.add(new BigInteger("13672554"));
        c.add(new BigInteger("10974047"));
        List<BigInteger> s = new ArrayList<>();
        List<BigInteger> m = new ArrayList<>();

        for (int i = 2; i < 6; i++) {
            BigInteger M = new BigInteger("0");
            for (int j = 2; j < 6; j++) {
                if (i != j) {
                    M = r.get(j).multiply(r.get(j).subtract(r.get(i)).modInverse(p)).mod(p);
                    m.add(M);
                }
            }
        }
        BigInteger s2 = m.get(0).multiply(m.get(1)).multiply(m.get(2)).mod(p);
        BigInteger s3 = m.get(3).multiply(m.get(4)).multiply(m.get(5)).mod(p);
        BigInteger s4 = m.get(6).multiply(m.get(7)).multiply(m.get(8)).mod(p);
        BigInteger s5 = m.get(9).multiply(m.get(10)).multiply(m.get(11)).mod(p);
        System.out.println("s2 = " + s2 + ", s3 = " + s3 + ", s4 = " + s4 + ", s5 = " + s5);

        BigInteger m2 = s2.multiply(c.get(2)).mod(p);
        BigInteger m3 = s3.multiply(c.get(3)).mod(p);
        BigInteger m4 = s4.multiply(c.get(4)).mod(p);
        BigInteger m5 = s5.multiply(c.get(5)).mod(p);

        System.out.println("cs2 = " + m2 + ", cs3 = " + m3 + ", cs4 = " + m4 + ", cs5 = " + m5);
        BigInteger M = m2.add(m3).add(m4).add(m5).mod(p);
        System.out.println("Decrypted message: " + M);
    }
}
