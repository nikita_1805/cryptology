package lab9;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class TaskTwo {
    public static void main(String[] args) {
        BigInteger p = new BigInteger("4375578271");
        List<BigInteger> r = new ArrayList<>();
        r.add(new BigInteger("11243"));
        r.add(new BigInteger("11251"));
        r.add(new BigInteger("11257"));
        r.add(new BigInteger("11261"));
        r.add(new BigInteger("11273"));
        r.add(new BigInteger("11287"));
        r.add(new BigInteger("11299"));
        r.add(new BigInteger("11289"));
        r.add(new BigInteger("11197"));
        r.add(new BigInteger("11489"));
        r.add(new BigInteger("11213"));
        r.add(new BigInteger("11239"));
        r.add(new BigInteger("12251"));
        r.add(new BigInteger("12253"));
        List<BigInteger> c = new ArrayList<>();
        c.add(new BigInteger("2894876724"));
        c.add(new BigInteger("3585190239"));
        c.add(new BigInteger("1706913111"));
        c.add(new BigInteger("864230287"));
        c.add(new BigInteger("2824249321"));
        c.add(new BigInteger("3368454117"));
        c.add(new BigInteger("126445471"));
        c.add(new BigInteger("1034022466"));
        c.add(new BigInteger("519388540"));
        c.add(new BigInteger("114862878"));
        c.add(new BigInteger("3678469168"));
        c.add(new BigInteger("545262864"));
        c.add(new BigInteger("1189362833"));
        c.add(new BigInteger("1713361503"));

        List<BigInteger> m = new ArrayList<>();

        for (int i = 2; i < 9; i++) {
            BigInteger M = new BigInteger("0");
            for (int j = 2; j < 9; j++) {
                if (i != j) {
                    M = r.get(j).multiply(r.get(j).subtract(r.get(i)).modInverse(p)).mod(p);
                    m.add(M);
                }
            }
        }
        BigInteger s2 = m.get(0).multiply(m.get(1)).multiply(m.get(2)).multiply(m.get(3)).multiply(m.get(4)).multiply(m.get(5)).mod(p);
        BigInteger s3 = m.get(6).multiply(m.get(7)).multiply(m.get(8)).multiply(m.get(9)).multiply(m.get(10)).multiply(m.get(11)).mod(p);
        BigInteger s4 = m.get(12).multiply(m.get(13)).multiply(m.get(14)).multiply(m.get(15)).multiply(m.get(16)).multiply(m.get(17)).mod(p);
        BigInteger s5 = m.get(18).multiply(m.get(19)).multiply(m.get(20)).multiply(m.get(21)).multiply(m.get(22)).multiply(m.get(23)).mod(p);
        BigInteger s6 = m.get(24).multiply(m.get(25)).multiply(m.get(26)).multiply(m.get(27)).multiply(m.get(28)).multiply(m.get(29)).mod(p);
        BigInteger s7 = m.get(30).multiply(m.get(31)).multiply(m.get(32)).multiply(m.get(33)).multiply(m.get(34)).multiply(m.get(35)).mod(p);
        BigInteger s8 = m.get(36).multiply(m.get(37)).multiply(m.get(38)).multiply(m.get(39)).multiply(m.get(40)).multiply(m.get(41)).mod(p);
        System.out.println("s2 = " + s2 + ", s3 = " + s3 + ", s4 = " + s4 + ", s5 = " + s5 + ", s6 = " + s6
        + ", s7 = " + s7 + ", s8 = " + s8);

        BigInteger m2 = s2.multiply(c.get(2)).mod(p);
        BigInteger m3 = s3.multiply(c.get(3)).mod(p);
        BigInteger m4 = s4.multiply(c.get(4)).mod(p);
        BigInteger m5 = s5.multiply(c.get(5)).mod(p);
        BigInteger m6 = s6.multiply(c.get(6)).mod(p);
        BigInteger m7 = s7.multiply(c.get(7)).mod(p);
        BigInteger m8 = s8.multiply(c.get(8)).mod(p);

        System.out.println("cs2 = " + m2 + ", cs3 = " + m3 + ", cs4 = " + m4 + ", cs5 = " + m5 + ", cs6 = " + m6
                + ", cs7 = " + m7 + ", cs8 = " + m8);

        BigInteger M = m2.add(m3).add(m4).add(m5).add(m6).add(m7).add(m8).mod(p);
        System.out.println("Decrypted message: " + M);
    }
}
